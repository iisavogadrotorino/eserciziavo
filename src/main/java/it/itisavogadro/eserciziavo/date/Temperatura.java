
package it.itisavogadro.eserciziavo.date;


public class Temperatura implements Comparable<Temperatura> {

	private float gradi;

	public float getGradi() {
		return gradi;
	}

	public void setGradi(float gradi) {
		if (gradi < -273)
			throw new RuntimeException("minimo -273");
		
		this.gradi = gradi;
	}

	private Temperatura() {}
	
	public Temperatura(float gradi) {
		setGradi(gradi);
	}
	
	boolean equals(Temperatura t) {
		return (this.getGradi() == t.getGradi());
	}

	@Override
	public int compareTo(Temperatura o) {
		if (o.getGradi() < this.getGradi())
			return -1;
		if (o.getGradi() > this.getGradi())
			return 1;
		return 0;
	}
	
}
